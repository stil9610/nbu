#include <iostream>
#include <string>

using namespace std;

int main(){
    cout << "Enter your firstname: ";
    string first_name;
    cin >> first_name;
    cout << endl << "Enter your lastname: ";
    string last_name;
    cin >> last_name;
    cout << endl << "Enter your address: ";
    string address;
    cin.ignore();
    getline(cin, address);
    cout << endl << "================" << endl << ">" << first_name << endl << ">" << last_name << endl << ">" << address << endl << "================" << endl;
return 0;
}
