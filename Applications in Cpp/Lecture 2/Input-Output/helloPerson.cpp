#include <iostream>
#include <string>

using namespace std;

int main()
{
    string first_name, last_name;
    cout << "Enter your first name:" << endl;
    cin >> first_name;
    cout << "Enter your last name:" << endl;
    cin >> last_name;
    cout << "Hello " << first_name << " " << last_name << endl;

    return 0;
}
