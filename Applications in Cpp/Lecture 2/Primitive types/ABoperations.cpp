#include <iostream>

using namespace std;

int main(){
    cout << "Enter a: ";
    int a;
    cin >> a;
    cout << "Enter b: ";
    int b;
    cin >> b;
    cout << "a + b = " << a+b << endl;
    cout << "a - b = " << a-b << endl;
    cout << "a * b = " << a*b << endl;
    cout << "a / b = " << (double)a/b << endl;
}
