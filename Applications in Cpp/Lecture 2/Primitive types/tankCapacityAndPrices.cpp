#include <iostream>

using namespace std;

int main()
{
    double tank_size, fuel_efficiency, price_per_liter;
    cout << "Enter a tank size:" << endl;
    cin >> tank_size;
    cout << "Enter fuel efficiency (km per liter):" << endl;
    cin >> fuel_efficiency;
    cout << "Enter price per liter:" << endl;
    cin >> price_per_liter;
    cout << "We can travel " << tank_size * fuel_efficiency << "km." << endl;
    cout << "For every 100km it will cost " << price_per_liter*(100/fuel_efficiency) << "lv." << endl;

    return 0;
}
