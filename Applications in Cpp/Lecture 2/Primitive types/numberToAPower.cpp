#include <cmath>
#include <iostream>

using namespace std;

int main () {
    cout << "Enter a number: ";
    int number;
    cin >> number;
    cout << number << "^2 = " << pow(number, 2) << ", ";
    cout << number << "^3 = " << pow(number, 3) << ", ";
    cout << number << "^4 = " << pow(number, 4) << ", ";
    cout << number << "^5 = " << pow(number, 5) << ", ";
}
