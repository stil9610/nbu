#include <iostream>

using namespace std;

int main()
{
    double price, amount;
    cout << "Enter price of something(lv):" << endl;
    cin >> price;
    cout << "Enter amount(lv): " << endl;
    cin >> amount;
    cout << "Your change is " << amount - price << "lv (" << (amount - price) *100 << "st)" << endl;
    return 0;
}
