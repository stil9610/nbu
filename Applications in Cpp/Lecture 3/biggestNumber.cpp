#include <iostream>
#include <string>

using namespace std;

int main(){
    double a;
    double b;
    double c;
    double max_val;
    cin >> a >> b >> c;
    max_val = a > b ? a : b;
    max_val = max_val > c ? max_val : c;
    cout << max_val;
}
