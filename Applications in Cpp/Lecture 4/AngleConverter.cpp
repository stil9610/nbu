#include <iostream>
#include <string>
#include <math.h>

using namespace std;

double convertDegreesToRadians(double degrees);
double convertRadiansToDegrees(double radians);

int main() {
    char radiansOfrDegrees;
    int value_to_convert;
    cin >> value_to_convert >> radiansOfrDegrees;
    if(radiansOfrDegrees == 'R')
        cout << convertRadiansToDegrees(value_to_convert) << endl;
    else if(radiansOfrDegrees == 'D')
        cout << convertDegreesToRadians(value_to_convert) << endl;
}

double convertDegreesToRadians(double degrees) {
    double radians;
    radians = (degrees / 180) * M_PI;
    return radians;
}
double convertRadiansToDegrees(double radians) {
    double degrees;
    degrees = (radians * 180) / M_PI;
    return degrees;
}
