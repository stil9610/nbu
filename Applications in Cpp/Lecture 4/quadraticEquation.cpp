#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c;
    cin >> a >> b >> c;
    double x1, x2;
    if(a != 0)
    {
        double discriminant = b*b - 4*a*c;
        if(discriminant < 0)
        {
            cout << "No real roots" << endl;
        }
        else if(discriminant == 0)
        {
            x1 = -b/(2*a);
            x2 = x1;
            cout << "The equation has only one real root: " << x1 << endl;
        }
        else
        {
            x1 = (-b + sqrt(discriminant))/2*a;
            x2 = (-b - sqrt(discriminant))/2*a;
            cout << "The equation has two real roots which are x1: " << x1 << " and x2: " << x2 << endl;
        }
    }
    else
    {
        cout << "The value of a should NOT be zero!" << endl;
    }
}
