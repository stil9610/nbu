#include <iostream>
#include <string>

using namespace std;

int main(){
    string first_name, last_name, faculty_number;
    double first_note, second_note, third_note;
    cin >> first_name >> last_name >> faculty_number >> first_note >> second_note >> third_note;
    if(first_note >= 2 && first_note <= 6 && second_note >= 2 && second_note <= 6 && third_note >= 2 && third_note <= 6){
        double average = (first_note + second_note + third_note) / 3;
        cout << faculty_number << " " << average;
    }else{
        cout << "Invalid data!" << endl;
    }
}
