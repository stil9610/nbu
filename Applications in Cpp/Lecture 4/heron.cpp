#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    double a, b, c;
    cin >> a >> b >> c;
    if(a > 0 && b > 0 && c > 0 && a < b+c && b < c + a && c < a+b){
        double perimeter = a+b+c;
        perimeter/=2;
        cout << perimeter << endl;
        double area = perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c);
        cout << area;
        area = sqrt(area);
        cout << area << endl;
    }
    else
    {
    cout << "Such a triangle does not exist!" << endl;
    }
}
