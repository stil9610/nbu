// AgregateFuncsOnVectors.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <vector>
#include <iostream>
#include <string>

using namespace std;

double sum(vector<double> numbers)
{
	double sum = 0;
	for (int i = 0; i < numbers.size(); i++)
	{
		sum += numbers.at(i);
	}
	return sum;
}

double average(vector<double> numbers)
{
	double average = 0;
	for (int i = 0; i < numbers.size(); i++)
	{
		average += numbers.at(i);
	}
	average /= numbers.size();
	return average;
}

double min(vector<double> numbers)
{
	double min = numeric_limits<double>::max();
	for (int i = 0; i < numbers.size(); i++)
	{
		if (min > numbers[i])
		{
			min = numbers[i];
		}
	}
	return min;
}

double max(vector<double> numbers)
{
	double max = numeric_limits<double>::min();
	for (int i = 0; i < numbers.size(); i++)
	{
		if (max < numbers[i])
		{
			max = numbers[i];
		}
		return max;
	}
}

int main()
{
	int numberOfValues;
	cin >> numberOfValues;
	vector<double> numbers;
	double current_number;
	for (int i = 0; i < numberOfValues; i++)
	{
		cin >> current_number;
		numbers.push_back(current_number);
	}
	string command;
	while (cin >> command)
	{
		if (command == "sum")
		{
			cout << sum(numbers) << endl;
		}
		else if (command == "avg")
		{
			cout << average(numbers) << endl;
		}
		else if (command == "min")
		{
			cout << min(numbers) << endl;
		}
		else if (command == "max")
		{
			cout << max(numbers) << endl;
		}
		else if (command == "ins")
		{
			cin >> current_number;
			numbers.push_back(current_number);
		}
		else if (command == "del")
		{
			cin >> current_number;
			numbers.erase(numbers.begin()+current_number, numbers.begin()+current_number+1);
		}
		else
		{
			cout << "No such command" << endl;
		}
	}
    return 0;
}

