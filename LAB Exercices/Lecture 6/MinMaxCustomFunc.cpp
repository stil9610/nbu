#include <iostream>
#include <limits>

using namespace std;

int get_min(int number, int second_number)
{
	return number > second_number ? second_number : number;
}

int get_max(int number, int second_number)
{
	return number > second_number ? number : second_number;
}

int main()
{
	int number;
	int min_number = numeric_limits<int>::max();
	int max_number = numeric_limits<int>::min();

	while (cin >> number)
	{
		min_number = get_min(min_number, number);
		max_number = get_max(number, max_number);
	}
	cout << min_number << " " << max_number << endl;
	return 0;
}

