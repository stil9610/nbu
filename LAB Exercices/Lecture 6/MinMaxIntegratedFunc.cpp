#include <iostream>
#include <limits>
#include <algorithm>

using namespace std;

int main()
{
	int number;
	int min_number = numeric_limits<int>::max();
	int max_number = numeric_limits<int>::min();

	while (cin >> number)
	{
		min_number = min(min_number, number);
		max_number = max(max_number, number);
	}
	cout << min_number << " " << max_number << endl;
	return 0;
}

