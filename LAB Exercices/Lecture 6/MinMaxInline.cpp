#include <iostream>
#include <limits>

using namespace std;

int main()
{
	int number;
	int min_number = numeric_limits<int>::max();
	int max_number = numeric_limits<int>::min();

	while (cin >> number)
	{
		if (number < min_number)
		{
			min_number = number;
		}
		if (number > max_number)
		{
			max_number = number;
		}
	}
	cout << min_number << " " << max_number << endl;
	return 0;
}

