#include "stdafx.h"
#include <iostream>
#include <cmath>

using namespace std;

double calculate_interest(double deposit, double interest, double period)
{
	return deposit*pow(1 + interest / 100, period);
}

int main()
{
	double deposit, interest, period;
	cin >> deposit >> interest >> period;
	cout << calculate_interest(deposit, interest, period) << endl;
	return 0;
}

