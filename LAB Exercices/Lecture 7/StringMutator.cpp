#include <iostream>
#include <string>

using namespace std;

string mutate_string(string text)
{
	bool isNumber = true;
	for (int i = 0; i < text.length(); i++)
	{
		if (text[i] < '0' || text[i] > '9')
		{
			isNumber = false;
			break;
		}
	}
	if (isNumber)
	{
		return text.append("[OK]");
	}
	else
	{
		return text.append("[FAIL]");
	}
}

int main()
{
	string text;
	cin >> text;
	cout << mutate_string(text) << endl;
	return 0;
}

