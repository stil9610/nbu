#include <iostream>
#include <cmath>

using namespace std;

double get_balance_with_complicated_interest(double balance, double interest, double period)
{
	return balance*pow(1 + interest / 100, period);
}
double get_balance_with_simple_interest(double balance, double interest, double period)
{
	return balance*(1 + interest*period / 100);

}

int main()
{
	double balance, interest, period;
	cin >> balance >> interest >> period;
	cout << "Simple interest: " << get_balance_with_simple_interest(balance, interest, period) << endl;
	cout << "Complicated interest: " << get_balance_with_complicated_interest(balance, interest, period) << endl;
	return 0;
}

