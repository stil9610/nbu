#include <iostream>

using namespace std;

double get_sum(double first, double second)
{
	return first + second;
}

double get_division(double first, double second)
{
	return first / second;
}

double get_multiplication(double first, double second)
{
	return first*second;
}

double get_subtraction(double first, double second)
{
	return first - second;
}

double solve_expression(double first_number, char operation, double second_number)
{
	double result;
	switch (operation)
	{	
	case '+':
		result = get_sum(first_number, second_number);
		break;
	case '-':
		result = get_subtraction(first_number, second_number);
		break;
	case '*':
		result = get_multiplication(first_number, second_number);
		break;
	case '/':
		result = get_division(first_number, second_number);
		break;
	default:
		break;
	}
	return result;
}

int main()
{
	double first_number, second_number;
	char operation;
	cin >> first_number >> operation >> second_number;
	cout << solve_expression(first_number, operation, second_number) << endl;
	return 0;
}

