#include <iostream>
#include <cmath>

using namespace std;

bool is_beautiful(int number)
{
	bool is_beautiful = true;
	int digit;
	while (number != 0)
	{
		digit = number % 10;
		if (digit % 2 == 1)
		{
			is_beautiful = false;
		}
		number /= 10;
	}
	return is_beautiful;
}

int main()
{
	int number;
	while (cin >> number)
	{
		if (number <= 1)
		{
			cout << "N/A" << endl;
		}
		else
		{
			if (is_beautiful(number))
			{
				cout << "YES" << endl;
			}
			else
			{
				cout << "NO" << endl;
			}
		}
	}
	return 0;
}

