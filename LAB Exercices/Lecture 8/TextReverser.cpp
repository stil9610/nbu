#include <iostream>
#include <string>

using namespace std;

void reverse_string(string* text)
{
	char exchange_character;
	for (int i = 0; i < text->length()/2; i++)
	{
		exchange_character = text->at(i);
		text->at(i) = text->at(text->length() - 1 - i);
		text->at(text->length() - 1 - i) = exchange_character;
	}
}

int main()
{
	string input;
	getline(cin, input);
	while (input != "exit")
	{
		reverse_string(&input);
		cout << input << endl;
		getline(cin, input);
	}
	return 0;
}

