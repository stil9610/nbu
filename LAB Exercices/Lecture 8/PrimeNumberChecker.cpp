#include <iostream>
#include <cmath>

using namespace std;

bool isPrime(int number)
{
	bool prime = true;
	for (int i = 2; i <= sqrt(number); i++)
	{
		if (number % i == 0)
		{
			prime = false;
			break;
		}
	}
	return prime;
}

int main()
{
	int number;
	while (cin >> number)
	{
		if (number <= 1)
		{
			cout << "N/A" << endl;
		}
		else
		{
			if (isPrime(number))
			{
				cout << "YES" << endl;
			}
			else
			{
				cout << "NO" << endl;
			}
		}
	}
	return 0;
}

