#include <iostream>
#include <string>

using namespace std;

int getValueForNote(char note);

int main()
{
    string note;
    cin >> note;
    double note_val;
    double bonus;
    note_val = getValueForNote(note[0]);
    if(note.length() != 1){
        if (note[1] == '+' && note[0] != 'A'){
            bonus = 0.3;
            note_val+=bonus;
        }
        else if (note[1] == '-' && note[0] != 'F'){
                bonus = -0.3;
                note_val += bonus;
            }

    }
    cout << note_val;
}

int getValueForNote(char note){
    switch (note){
        case 'A':
            return 4;
        case 'B':
            return 3;
        case 'C':
            return 2;
        case 'D':
            return 1;
        case 'F':
            return 0;
    }
}
