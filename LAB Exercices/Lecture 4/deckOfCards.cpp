#include <iostream>
#include <string>

using namespace std;
string getNameOfNumberAndSuit(string pairr);

int main()
{
    string pair_of_number_suit;
    cin >> pair_of_number_suit;
    string name_number_suit = getNameOfNumberAndSuit(pair_of_number_suit);
    cout << name_number_suit;
}
string getNameOfNumberAndSuit(string pairr)
{
    char value_of_card = pairr[0];
    char suit_of_card = pairr[1];
    string final_output_string = "";
    switch(value_of_card) {
        case '2':
            final_output_string += "Two";
            break;
         case '3':
            final_output_string += "Three";
            break;
         case '4':
            final_output_string += "Four";
            break;
         case '5':
            final_output_string += "Five";
            break;
         case '6':
            final_output_string += "Six";
            break;
         case '7':
            final_output_string += "Seven";
            break;
         case '8':
            final_output_string += "Eight";
            break;
         case '9':
            final_output_string += "Nine";
            break;
         case '10':
            final_output_string += "Ten";
            break;
        case 'J':
            final_output_string += "Jack";
            break;
         case 'Q':
            final_output_string += "Queen";
            break;
        case 'K':
            final_output_string += "King";
            break;
         case 'A':
            final_output_string += "Ace";
            break;
    }
    final_output_string += " of ";
    switch(suit_of_card){
        case 'D':
            final_output_string += "diamonds";
            break;
        case 'H':
            final_output_string += "hearts";
            break;
        case 'S':
            final_output_string += "spades";
            break;
        case 'C':
            final_output_string += "clubs";
            break;
    }
    return final_output_string;
}
