#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

bool string_contains_pattern(string text, string pattern)
{
	bool text_contains_pattern = false;
	for (int i = 0; i <= text.length() - pattern.length(); i++)
	{
		if (text.substr(i, pattern.length()) == pattern)
		{
			text_contains_pattern = true;
			break;
		}
	}
	return text_contains_pattern;
}

int main()
{
	string pattern;
	cin >> pattern;
	string word;
	while (cin >> word)
	{
		if (string_contains_pattern(word, pattern))
		{
			cout << "YES" << endl;
		}
		else
		{
			cout << "NO" << endl;
		}
	}
	return 0;
}

