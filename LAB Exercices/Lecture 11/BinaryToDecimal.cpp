#include <iostream>
#include <cmath>


using namespace std;

int convert_binary_to_decimal(int binary_number)
{
	int decimal_number = 0;
	int counter = 0;
	while (binary_number != 0)
	{
		decimal_number += binary_number % 10 * pow(2, counter);
		counter++;
		binary_number /= 10;
	}
	return decimal_number;
}

int main()
{
	int input_number;
	cin >> input_number;
	cout << convert_binary_to_decimal(input_number) << endl;
	return 0;

}

