#include <iostream>

using namespace std;

int get_even_digits_from_number(int number)
{
	int even_digits = 0;
	int digit;
	while (number != 0)
	{
		digit = number % 10;
		if (digit % 2 == 0)
		{
			even_digits++;
		}
		number /= 10;
	}
	return even_digits;
}

int get_odd_digits_from_number(int number)
{
	int odd_digits = 0;
	int digit;
	while (number != 0)
	{
		digit = number % 10;
		if (digit % 2 == 1)
		{
			odd_digits++;
		}
		number /= 10;
	}
	return odd_digits;
}

int main()
{
	int number_of_inputs;
	cin >> number_of_inputs;
	int number_of_even_digits;
	int number_of_odd_digits;
	int input_number;
	for (int i = 0; i < number_of_inputs; i++)
	{
		cin >> input_number;
		number_of_even_digits = get_even_digits_from_number(input_number);
		number_of_odd_digits = get_odd_digits_from_number(input_number);
		cout << number_of_odd_digits << " " << number_of_even_digits << endl;
	}
	return 0;

}

